//
//  UIImage+Extension.swift
//  XS微博
//
//  Created by 王小帅 on 2016/11/29.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import Foundation
import UIKit


// MARK: - 给图片做扩展方法，目的是为了优化图片缩放，圆角，透明度的性能
extension UIImage {
    
    /// 自定义绘制头像图像方法 对图像显示性能做优化
    ///
    /// - Parameters:
    ///   - size: 绘图尺寸
    ///   - isRatio: 是否圆角 默认false
    ///   - backColor: 背景颜色，因为提升性能的同时要将图片设置为不透明，这样裁切会有黑边，所以设置背景颜色来解决
    ///   - lineColor: 边框的颜色 
    ///   - lineWidth: 边框的宽度 默认 1
    func xs_avatarImage(size: CGSize?, isRatio:Bool = true, backColor: UIColor = UIColor.white, lineColor: UIColor = UIColor.darkGray, lineWidth: CGFloat = 1.0)-> UIImage? {
        
        // 非空处理
        var size = size
        if size == nil {
            size = self.size
        }
        
        // rect
        let rect = CGRect(origin: CGPoint(), size: size!)
        
        // 开启图片等上下文
        /**
          参数：
          1. 绘图尺寸
          2. false(透明) true(不透明)
          3. 分辨率 0 自动设置当前设备等分辨率  默认 1  质量低
         */
        UIGraphicsBeginImageContextWithOptions(rect.size, isRatio, 0)
        
        // 背景颜色填充
        backColor.setFill()
        UIRectFill(rect)
        
        // 绘制圆角
        if isRatio {
            // 获取图片的path
            let path = UIBezierPath(ovalIn: rect)
            // 裁切
            path.addClip()
        }else {
            let path = UIBezierPath(rect: rect)
            path.addClip()
        }
        
        // 绘图
        draw(in:rect)
        
        // 设置边界线
        if isRatio {
            // 圆形path
            let ovalPath = UIBezierPath(ovalIn: rect)
            // 设置属性
            ovalPath.lineWidth = lineWidth
            lineColor.setStroke()
            ovalPath.stroke()
        }
        
        // 获取结果
        let result = UIGraphicsGetImageFromCurrentImageContext()
        
        // 关闭上下文
        UIGraphicsEndImageContext()
        
        // 返回结果
        return result
    }
}
