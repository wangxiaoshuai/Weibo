//
//  UIImageView+WebImage.swift
//  XS微博
//
//  Created by 王小帅 on 2016/11/29.
//  Copyright © 2016年 王小帅. All rights reserved.
//
import UIKit
import SDWebImage

extension UIImageView{
    // 定义自己的绘图图像方法 隔离sdwebimage
    func xs_setImage(urlStr:String?, palceholder:UIImage?, isAvatar:Bool = false){
        
        // 没有获取到图片就设置展位图
        guard let url = URL(string: urlStr!) else {
            image = palceholder
            return
        }
        
        sd_setImage(with: url, placeholderImage: palceholder, options: [], completed:{
            [weak self] (image,_,_,_) in
            
            // 如果是头像就会知圆角
            if isAvatar {
                self?.image = self?.image?.xs_avatarImage(size: self?.bounds.size)
            }else{
//                self?.image = self?.image?.xs_avatarImage(size: image?.size, isRatio: false)
            }
            
        })
    }

}
