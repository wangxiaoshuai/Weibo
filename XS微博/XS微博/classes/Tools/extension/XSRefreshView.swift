//
//  XSRefreshView.swift
//  XS微博
//
//  Created by 王小帅 on 2016/12/12.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import UIKit


/// 下拉刷新的view： 对刷新时候的界面做操作
class XSRefreshView: UIView {

    // 当前所处的状态 设置为计算性属性 当状态更改了 对空间做操作
    var refreshStatus:XSRefreshStatus = .Normal {
        didSet{
            switch refreshStatus {
            case .Normal:
                // 回复控件
                tipIcon.isHidden = false
                indicator.stopAnimating()
                tipMsg.text = "下拉准备刷新..."
                // 图标还原
                UIView.animate(withDuration: 0.25, animations: { 
                    self.tipIcon.transform = CGAffineTransform.identity
                })
            case .Pulling:
                tipMsg.text = "松手开始刷新..."
                //更改图标
                UIView.animate(withDuration: 0.25, animations: {
                    self.tipIcon.transform = CGAffineTransform(rotationAngle: CGFloat(-M_PI + 0.001))
                })
            case .WillRefresh:
                tipMsg.text = "正在努力刷新..."
                // 显示菊花
                tipIcon.isHidden = true
                indicator.startAnimating()
                
            }
        }
    }
    
    
    /// 下拉图标
    @IBOutlet weak var tipIcon: UIImageView!
    /// 下拉提示信息
    @IBOutlet weak var tipMsg: UILabel!
    /// 下拉刷新图标
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    
    /// 方便外界调用的类初始化方法
    ///
    /// - Returns: xib创建的实例
    class func refreshView() -> XSRefreshView{
        
        let nib = UINib(nibName: "XSRefreshView", bundle: nil)
        
        return nib.instantiate(withOwner: nil, options: nil)[0] as! XSRefreshView
    }
    
    
}
