//
//  Bundle+Extension.swift
//  XS微博
//
//  Created by 王小帅 on 2016/11/6.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import Foundation

extension Bundle{
    
    // 项目的命名空间
    var namespace:String {
        
        return infoDictionary?["CFBundleName"] as? String ?? " "
    }
    
}
