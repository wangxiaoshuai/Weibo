//
//  Label+Extension.swift
//  XS微博
//
//  Created by 王小帅 on 2016/11/13.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import UIKit

extension UILabel {
    
    convenience init(txt:String,fontSize:CGFloat, align:NSTextAlignment, lines:Int = 0, fontColor: UIColor){
    
        self.init()
        self.text = txt
        self.font = UIFont(name: self.font.fontName, size: fontSize)
        self.textAlignment = align
        self.numberOfLines = lines
        self.textColor = fontColor
    
    }
}
