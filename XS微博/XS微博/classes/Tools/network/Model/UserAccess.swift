//
//  UserAccess.swift
//  XS微博
//
//  Created by 王小帅 on 2016/11/22.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import Foundation
import YYModel

/// 保存用户信息
class UserAccess: NSObject {
    
    // 文件名字
    let accessFile = "userAccess.josn"
    
    // 请求令牌
    var access_token: String?// = "2.00TbIoYCguvGXBc535157ec15qYhrC"
    // 授权用户的id
    var uid: String?
    // 过期的秒数
    var expires_in: TimeInterval = 0 {
        didSet{
            expiresDate = Date(timeIntervalSinceNow: expires_in)
        }
    }
    
    // 应为expires_in 是秒数，所以要定义一个date表示具体的过期时间
    // 并且在expires_in被设置的时候 自动设置它
    var expiresDate: Date?
    
    // 用户昵称
    var screen_name: String?
    // 头像	用户头像地址（大图），180×180像素
    var avatar_large: String?
    
    // 要是用yy_model的功能要重写属性
    override var description: String{
    
        return yy_modelDescription()
    }
    
    
    /// MARK: - 这里注意 因为其他地方判断是否登录都是靠的懒加载本类 检车accessToken是否有值 所以重写init方法等时候去磁盘取
    override init() {
        super.init()
        
        // 从沙河读取数据
        let filePath = accessFile.documentUrl
        guard let data = NSData(contentsOfFile: filePath),
        // 使用yymodel 需要一个json的字典
        let dict = try? JSONSerialization.jsonObject(with: data as Data, options: [])
            else {
                return
        }
        
        // 赋值给self
        _ = yy_modelSet(withJSON: dict)
        
        // 测试过期
//        expiresDate = Date(timeIntervalSinceNow: (-360 * 24))
        
        // 判断是否过期
        if expiresDate?.compare(Date()) != ComparisonResult.orderedDescending {
            // 清空self
            self.access_token = nil
            self.uid = nil
        }
        
    }
    
    
    
    // 存沙盒
    // 存储为json 到文档目录下
    func saveUserAccess(){
        
        // 首先注意：expiresDate是固定不变的 expires_in每次请求都是距离过期时间的秒数所以我们只存储前者
        // 利用yymodel的方法获取data
        var jsonData = (self.yy_modelToJSONObject() as? [String: AnyObject]) ?? [:]
        // 删除expires_in
        jsonData.removeValue(forKey: "expires_in")

        // 获取userAccessjson
        guard let accessData = try? JSONSerialization.data(withJSONObject: jsonData, options: []) else {
        
            print("UserAccess.josn 写入本地沙盒 失败 ！")
            return
        }
        
        // 获取沙盒文档路径
        let filePath = accessFile.documentUrl
        
        // 写入沙盒用oc的方法更简单顺手
        (accessData as NSData).write(toFile: filePath, atomically: true)
        print("UserAccess.josn 写入本地沙盒 成功 ！\(filePath)")
        
    }
    
    
    
    
    
    
    
}
