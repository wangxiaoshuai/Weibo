//
//  XSNetworkTools.swift
//  XS微博
//
//  Created by 王小帅 on 2016/11/16.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import UIKit
import Alamofire
import YYModel


/// 请求微博数据工具类
class XSNetworkTools {

    static let shared = XSNetworkTools()
    lazy var userAccess = UserAccess()
    /// 设置用户登录状态 区分游客 判断依据 是否存在accesstoken
    // 改为计算型属性, 如果非登录状态，那么最开始的请求服务器等草族都要停止（timer也不能再访问）
    public var userLogon: Bool {
        
        return userAccess.access_token != nil
    }

}


// MARK: - 获取用户信息
extension XSNetworkTools {

    /// 加载用户信息字典
    func loadUserInfo(completion:@escaping ([String: AnyObject])->()) {
        
        guard let uid = self.userAccess.uid else {
            return
        }
        // url
        let url = "https://api.weibo.com/2/users/show.json"
        // paramers
        let paramers = ["uid": uid]
        
        // request
        tokenRequest(url: url, paramers: paramers as [String : AnyObject]?, completion:
            { (json, isSuccess) in
                
                completion(json as! [String : AnyObject])
        })
    }
}


///  负责请求微博数据
extension XSNetworkTools{

    func unReadCount(completion: @escaping (_ count: Int)->()) {
        let unreadUrl = "https://rm.api.weibo.com/2/remind/unread_count.json"
       
        let paramers = ["uid": userAccess.uid as AnyObject, "unread_message": 1 as AnyObject]
        // 注意 api中有提及到，如果是授权登录的话 token是必须的参数
        tokenRequest(url: unreadUrl, paramers: paramers) { (json, isSuccess) in
        
            guard json != nil else {
            
                completion(0)
                return
            }
            
            let count = json!["status"]
            completion(count as! Int)
            
        }
    }
    
    /// 获取微博数据列表
    ///
    /// - Parameters:
    ///   - since_id: 默认0 有值则返回大于该值的微博即最新的
    ///   - max_id: 默认0 有值则返回小于等于该值的微博 即以前的微博
    ///   - completion: 回调
    func statusListRequest(since_id:Int64 = 0, max_id:Int64 = 0,completion: @escaping (_ json:AnyObject?, _ isSuccess: Bool)->()) {
        
        // 请求新浪微博数据
        let sinaUrl = "https://api.weibo.com/2/statuses/home_timeline.json"
        
        // 这里注意：如果maxid 有值 返回ID小于或等于max_id的微博
        // 所以会产生一条重复的数据 所以maxid要-1
        let maxID = max_id > 0 ? (max_id - 1) : 0
        let parameters = ["since_id": (since_id as NSNumber), "max_id": (maxID as NSNumber)]
        
        tokenRequest(url: sinaUrl, paramers: parameters, completion: completion)
    }
    
    
    
    
    
    /// 该方法负责request请求，默认包含assessToken
    ///
    /// - Parameters:
    ///   - url: 请求的路径
    ///   - reqMethod: 请求方法 默认 GET
    ///   - paramers: 默认包含assessToken
    ///   - completion: 回调函数
    func tokenRequest(url: String, reqMethod: HTTPMethod = HTTPMethod.get, paramers: [String: AnyObject]?, completion: @escaping (_ statuslit: AnyObject?, _ isSucess:Bool)->()) {
        
        // 因为token是可选的（用户登录的时候还没有token），所以如果token不存在则不能继续请求网络数据
        // 这里的guard 守护的写法与之前的版本不同，系统会提示将  let XX = XXX as? XXX 改成 XX != nil
        guard userAccess.access_token != nil else {
            // 没有token，直接返回
            completion(nil, false)
            return
        }
        
        // 判断是否有参数，并且将token添加到参数列表
        var paramers = paramers
        if paramers == nil {
            paramers = [String: AnyObject]()
        }
        
        // 加入token
        paramers?["access_token"] = userAccess.access_token as AnyObject?
        
        // 调用基础请求
        request(url: url, reqMethod: reqMethod, paramers: paramers, completion: completion)
        
    }

    
    /// 基础request请求方法
    ///
    /// - Parameters:
    ///   - url: 请求的路径
    ///   - reqMethod: 请求方法 默认 GET
    ///   - paramers: 默认包含assessToken
    ///   - completion: 回调函数
    ///   注意： request 的后面参数不用传，如果你知道要返回的数据格式可以传。否则会有错误403
    func request(url: String, reqMethod: HTTPMethod = HTTPMethod.get, paramers: [String: AnyObject]?, completion: @escaping (_ statuslit: AnyObject?, _ isSucess:Bool)->()) {
        
        // 发起请求
        Alamofire.request(url, method: reqMethod, parameters: paramers).validate().responseJSON(completionHandler: { (_ response) in
            
            // 判断请求状态，做相应处理
            switch response.result {
            
            case .success(let json):
                completion(json as AnyObject, true)
                
            case .failure(let error):
                print(error)
                completion(nil, false)
            }
        })
    }
}


// MARK: - 负责请求accessToken
extension XSNetworkTools {

    
    /// 请求AccessToke
    ///
    /// - Parameter codeStr: 服务器返回的code码
    func getAccessToken(codeStr: String, completion:  @escaping (_ isSuccess: Bool)->()){
        
        // 获取token的url
        let tokenUrl = "https://api.weibo.com/oauth2/access_token"
        // 参数字典
        let parmars = ["client_id":APPClientId,
                       "client_secret": AppSecret,
                       "grant_type": "authorization_code",
                       "code": codeStr,
                       "redirect_uri": APPRedireUri]
        // post请求
        XSNetworkTools.shared.request(url: tokenUrl, reqMethod: .post, paramers: parmars as [String : AnyObject]?, completion: {(json, isSuccess) in
            
//            print(json ?? " ")
            
            // 下面要做的是将json装成userAccess，再将userAccess 存入沙盒。 
            // 这样程序关闭后不会丢失，只是每次加载的时候要判断下国旗时间
            // 存沙盒和从沙盒里取数据 都定义到userAccess中 这样逻辑合理
            
            // 存储json 为userAccess
            self.userAccess.yy_modelSet(withJSON: ((json as? [String: AnyObject]) ?? [:]))
            // 在用户授权成功后 获取用户信息
            self.loadUserInfo(completion: { (dict) in
                // 设置用户属性
                self.userAccess.yy_modelSet(withJSON: dict)
                
                // 保存到沙盒
                self.userAccess.saveUserAccess()
                // 注意这里要等用户信息保存完毕后在回调 否则取不到用户名字 返回结果
                completion(isSuccess)
                
            })
        })
        
    }

}




