//
//  EmoticonModel.swift
//  Swift图文混排
//
//  Created by 王小帅 on 2016/12/26.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import UIKit
import YYModel

/// 表情模型
class EmoticonModel: NSObject {

    /// 表情简体中文名字 发送给服务器的
    var chs: String?
    /// 表情的本地路径 供本地图文混排用
    var png: String?
    /// 表情的类型  false - 图片  | true - emoji 的十六进制编码
    var type = false
    /// emoji的十六进制编码
    var code: String?
    
    /// 表情等父目录
    var dir:String?
    
    /// 表情等图片
    var image:UIImage?{
        
        // 判断类型
        if type {
            return nil
        }
        
        // 设置目录等时候去设置图片
        guard  let dir = dir,
            let png = png,
            let path = Bundle.main.path(forResource: "Emoticons.bundle", ofType: nil),
            let bundle = Bundle(path: path)
            
            else {
                return nil
        }
        
        return UIImage(named: "\(dir)/\(png)", in: bundle, compatibleWith: nil)
    }

    
    /// 获取表情等属性文本
    func imgTxt(font: UIFont) -> NSAttributedString {
        
        // 判断是否有图片
        guard let image = image else {
            return NSAttributedString(string: " ")
        }
        
        // 文本附件
        let attachment = NSTextAttachment()
        attachment.image = image
        let height = font.lineHeight
        attachment.bounds = CGRect(x: 0, y: -4, width: height, height: height)
        
        return NSAttributedString(attachment: attachment)
    }

    
    override var description: String {
        return yy_modelDescription()
    }
}
