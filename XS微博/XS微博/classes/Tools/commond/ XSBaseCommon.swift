//
//   XSBaseCommon.swift
//  XS微博
//  类似OC中的宏  定义一些全局属性
//  Created by 王小帅 on 2016/11/19.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import Foundation
import UIKit


/// 全局状态栏状态表示
public var IsHidedStatusBar = false
/// 新特性和欢迎界面移除通知
public let NewFeatureAndWeclcomRemoveFromSuperView = "NewFeatureAndWeclcomRemoveFromSuperView"

/// 定义全局的通知
public let XSUserLoginNotifitation = "XSUserLoginNotifitation"

/// 用户授权登录成功通知
public let XSUserLoginSuccessNotifitation = "XSUserLoginSuccessNotifitation"

/// APPClientId : 申请应用时分配的AppKey。
let APPClientId = "1405404510"

/// APPRedireUri : 授权回调地址，站外应用需与设置的回调地址一致，站内应用需填写canvas page的地址
let APPRedireUri = "http://www.baidu.com"

/// APPSecret : 申请应用时分配的AppSecret
let AppSecret = "29faa94821755de8e6245e0debabadcb"

/// MARK: - 微博配图常量
/// 微博配图视图的外间距
let XSStatusPicsViewOutterMargin = CGFloat(12)
/// 微博配图视图的内部间距
let XSStatusPicsViewInnerMargin = CGFloat(8)
/// 微博配图视图的宽度
let XSStatusPicsViewWidth = CGFloat(UIScreen.main.bounds.width - 2 * XSStatusPicsViewOutterMargin)
/// 微博配图视图内部的子视图的宽度（正方形 宽==高）
let XSStatusPicsViewItemWH = CGFloat(XSStatusPicsViewWidth - 2 * XSStatusPicsViewInnerMargin) / 3

