//
//  XSStatusToolBar.swift
//  XS微博
//
//  Created by 王小帅 on 2016/11/30.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import UIKit

class XSStatusToolBar: UIView {

    // 微博视图模型
    var statusViewModel: StatusViewModel? {
        didSet{
        
            commonBtn?.setTitle(statusViewModel?.commonStr, for: [])
            reteewtBtn?.setTitle(statusViewModel?.reteewtStr, for: [])
            likeBtn?.setTitle(statusViewModel?.likeStr, for: [])
        }
    }
    
    /// 评论按钮
    @IBOutlet weak var commonBtn: UIButton?
    /// 转发按钮
    @IBOutlet weak var reteewtBtn: UIButton?
    /// 点赞按钮
    @IBOutlet weak var likeBtn: UIButton?

}
