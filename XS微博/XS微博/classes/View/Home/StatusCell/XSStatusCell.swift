//
//  XSStatusCell.swift
//  XS微博
//
//  Created by 王小帅 on 2016/11/29.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import UIKit

// 点击链接跳转是控制器的事  这里指发送协议
@objc protocol XSStatusCellDelegate: NSObjectProtocol {

    @objc optional func xSStatusCellSelectedUrlString(cell: XSStatusCell, urlString: String)
}

class XSStatusCell: UITableViewCell {
    
    // 代理
    weak var urlSelectedDelegate: XSStatusCellDelegate?
    
    // 设置一个属性来接收单个微博数据
    var statusModel:StatusViewModel? {
        didSet{
            // 征文
            statusLabel.attributedText =  statusModel?.statusAttrText
            // 设置转发微博的正文
            retweetedText?.attributedText = statusModel?.retweetAttrText
            
            // 用户名
            nameLabel.text = statusModel?.status.user?.screen_name
            // 设置会员名字颜色
            nameLabel.textColor = statusModel?.userNameColor
            // 会员图标
            memberIcon.image = statusModel?.memberIcon
            // vip图标
            vioIcon.image = statusModel?.verifiedIcon
            // 头像
            iconView.xs_setImage(urlStr: statusModel?.status.user?.profile_image_url, palceholder: UIImage(named: "avatar_default_big"), isAvatar: true)
            // 设置底部工具栏
            toolBar.statusViewModel = statusModel
            
            // 设置微博配图视图的高度
            statusPicsView.viewModel = statusModel

            // 设置微博配图数据
            statusPicsView.urls = statusModel?.picUrls
     
            // 微博来源
            sourceLabel.text = statusModel?.statusSource ?? "未知星球"
            // 微博日期
            timeLabel.text = "以前"
        }
    }


    
    /// 头像
    @IBOutlet weak var iconView: UIImageView!
    
    /// 昵称
    @IBOutlet weak var nameLabel: UILabel!
    
    /// 发布日期
    @IBOutlet weak var timeLabel: UILabel!
    
    /// 微博来源
    @IBOutlet weak var sourceLabel: UILabel!
    
    /// 会员标志
    @IBOutlet weak var memberIcon: UIImageView!
    
    /// vip认证标志
    @IBOutlet weak var vioIcon: UIImageView!
    /// 微博正文
    @IBOutlet weak var statusLabel: TFLabel!
    
    /// 底部工具栏
    @IBOutlet weak var toolBar: XSStatusToolBar!
    
    /// 微博配图设视图
    @IBOutlet weak var statusPicsView: XSStatusPicsView!
    
    
    /// 注意 原创微博是没有它的  所以要用 ？
    @IBOutlet weak var retweetedText: TFLabel?
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // 离屏渲染 - 异步绘制cell
        self.layer.drawsAsynchronously = true
        // 栅格化 - 异步绘制cell 后生成一张图 相当于 图层只有一个 滚动快 但是要设置分辨率
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        
        // 设置代理 监听连接点击
        statusLabel.delegate = self
        retweetedText?.delegate = self
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


// MARK: - 微博中的连接点击
extension XSStatusCell: TFLabelDelegate {

    // 微博中的网址链接被点击了
    func tFLabelDidSelectedUrlString(label: TFLabel, urlString: String) {
        urlSelectedDelegate?.xSStatusCellSelectedUrlString?(cell: self, urlString: urlString)
    }
    
}




