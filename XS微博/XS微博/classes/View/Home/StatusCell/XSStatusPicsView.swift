//
//  XSStatusPicsView.swift
//  XS微博
//
//  Created by 王小帅 on 2016/12/1.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import UIKit

class XSStatusPicsView: UIView {
    
    
    // 定义计算性属性 来动态的更改单张照片到尺寸
    var viewModel: StatusViewModel? {
        didSet{
            updateSignalPicSize();
        }
    }
    
    /// 更新单张图片的尺寸
    private func updateSignalPicSize(){
    
        // 单张
        if viewModel?.picUrls?.count == 1 {
        
           let vm = subviews[0]
            
            vm.frame = CGRect(x: 0,
                              y: XSStatusPicsViewOutterMargin,
                              width: (viewModel?.picsViewSize.width)!,
                              height: (viewModel?.picsViewSize.height)! - XSStatusPicsViewOutterMargin)
            
        }else{
            // 多张的话 要把第一个图片等尺寸改回来哦
            let vm = subviews[0]
            vm.frame = CGRect(x: 0,
                              y: XSStatusPicsViewOutterMargin,
                              width: XSStatusPicsViewItemWH,
                              height: XSStatusPicsViewItemWH)
        }
        
        // 修改高度约束
        picViewHeight?.constant = viewModel?.picsViewSize.height ?? 0
        
    }
    
    // 配图url数组
    var urls: [XSStatusPicsModel]?{
        didSet{
        
            // 因为9张配图的站位图已经有了先设为隐藏
            for item in subviews{
                item.isHidden = true
            }
            
            // 设置角标
            var index = 0
            
            // 循环设置网络图片并设置可见性
            for url in urls ?? [] {
                
                // 获取中图地址(开始用素略图很不清晰)
                let midUrl = url.thumbnail_pic
                // 获取单个配图
                let item = subviews[index] as! UIImageView
                
                // 展位图
                let placeholder = UIImage(named: "avatar_default_big")
                // 设置网络图
                item.xs_setImage(urlStr: midUrl, palceholder: placeholder, isAvatar: false)
                
                // 设置可见
                item.isHidden = false
                item.contentMode = .scaleAspectFill
                item.clipsToBounds = true
                item.isUserInteractionEnabled = true
                // 对4张图片等作处理
                if urls?.count == 4 && index == 1{
                    index += 1
                }
          
                // 角标递增
                index += 1
            }
        }
    }
    
    @IBOutlet weak var picViewHeight: NSLayoutConstraint?
    
    override func awakeFromNib() {
        // 设置配图
        setupUI();
    }
    
    
}


// MARK: - 设置微博配图的尺寸排版等属性
extension XSStatusPicsView{
    
    /// 这个方法里会创建9张配图出来，但是在上面didset的时候先隐藏掉，需要显示几张再显示。
    fileprivate func setupUI(){
        
        // 将超出编辑的部分咔嚓掉
        clipsToBounds = true
        // 这里设置视图的背景色为父视图的背景色
        backgroundColor = superview?.backgroundColor
        
        // 第一张配图的位置，定义好后以后新的配图以它为参照做offset
        let rect = CGRect(x: 0, y: XSStatusPicsViewOutterMargin, width: XSStatusPicsViewItemWH, height: XSStatusPicsViewItemWH)
        
        let count = 9
        for i in 0..<count {
            
            // 计算行和列来判断照片的坐标
            // 九宫格的行列基本都这个套路啊
            let row = i / 3
            let col = i % 3
            
            // x的偏移量
            let xOffSet = (XSStatusPicsViewItemWH + XSStatusPicsViewInnerMargin) * CGFloat(col)
            // y的偏移量
            let yOffSet = (XSStatusPicsViewItemWH + XSStatusPicsViewInnerMargin) * CGFloat(row)
            
            // 图片
            let iv = UIImageView()
//            iv.layer.contentsScale  = 2.0
            // 设置图片缩放模式
//            iv.contentMode = .scaleAspectFill
//            iv.clipsToBounds = true
//            iv.isUserInteractionEnabled = true
            // 设置frame
            iv.frame = rect.offsetBy(dx: xOffSet, dy: yOffSet)
            
            addSubview(iv)
        }
    }
}
