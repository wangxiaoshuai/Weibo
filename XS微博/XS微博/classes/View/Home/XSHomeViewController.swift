//
//  XSHomeViewController.swift
//  XS微博
//
//  Created by 王小帅 on 2016/11/6.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import UIKit
import Alamofire

class XSHomeViewController: XSBaseViewController {

    // 数据源数组
    private lazy var dataSource = [String]()
    // 这是原创微博的cellid
    fileprivate var originalCellID = "originalCellID"
    // 这是包含被转发微博的cellid
    fileprivate var retweetedCellID = "retweetedCellID"
    // 懒加载微博数组model
    lazy var statuses = StatusListViewModel()
    
    
    
    // 重写父类的数据源方法
    override func loadData() {
        
        self.refresh?.beginRefreshing()
        
       //  调用模型获取数据方法，将是否为上拉刷新作为参数传递
        statuses.loadStatusList(pullUp: isPullUp) { (isSuccess, shouldReloadData) in
            
            let daytime = self.isPullUp ? 0.5 : 0.0
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + daytime){
                
                
                // 不管成功与否都停止刷新
                self.refresh?.endRefreshing()
                
                // 如果有数据在刷新列表
                if shouldReloadData {
                    // 刷新表格
                    self.tableView?.reloadData()
                }
            }

        }
        
    }
    
    // 手动刷新
    override func beginRefresh(){
        
        // tableview要偏移的量
        var offSetH: CGFloat = 64
        
        // 这里要减去状态栏和标题栏的高度
        if let refH = (refresh?.bounds.height) {
            offSetH += refH
        }
        
        tableView?.setContentOffset(CGPoint(x:0,y:-(offSetH)), animated: true)
        // 模拟延时 让用户看到小菊花
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.0){
            // 手动刷新
            self.loadData()
            // 重置标记
            self.isPullUp = false
        }
    }
    
    // 刷新完成方法
    override func refreshCompleted() {
        
        //  做上拉和下拉的判断
        self.loadData()
        // 重置标记
        self.isPullUp = false
    }
    

    @objc fileprivate func showfinds()  {
        
        let vc = XSDemoViewController()
        
        navigationController?.pushViewController(vc, animated: true)
        
    }
}


// MARK: - 设置UI
extension XSHomeViewController{
    
    /// 重写父类等方法
    override func setUpUI() {
        super.setUpUI()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "好友", style: .plain, target: self, action: #selector(showfinds))
        
        // 这册cell
//        tableView?.register(UITableViewCell.self, forCellReuseIdentifier: cellID)
       
        // 注册原创和被撞发微博  在设置数据源的时候判断用哪个
        let originalNib = UINib(nibName: "XSStatusNormalCell", bundle: nil)
        tableView?.register(originalNib, forCellReuseIdentifier: originalCellID)
        
        let retweetedNib = UINib(nibName: "XSStatusRetweetedCell", bundle: nil)
        tableView?.register(retweetedNib, forCellReuseIdentifier: retweetedCellID)

        
        // 设置tableview的高度和预选高度 缓存了行高 自动行高就注销掉
//        tableView?.rowHeight = UITableViewAutomaticDimension
//        tableView?.estimatedRowHeight = 300
        
        // 清楚分割线
        tableView?.separatorStyle = .none
        
        // 设置标题按钮
        setUpTitleView()
    }
    
    
    /// 设置标题按钮
    private func setUpTitleView(){
        // 设置title
        let titleBtn = titleButton(title: XSNetworkTools.shared.userAccess.screen_name)
        
        titleBtn.addTarget(self, action: #selector(clickTitle(btn:)), for: .touchUpInside)
        navigationItem.titleView = titleBtn

    }
    
    
    /// 点击标题
    ///
    /// - Parameter btn: 被电击的标题
    @objc private func clickTitle(btn: UIButton){
    
        btn.isSelected = !btn.isSelected
    }
}


// MARK: - 设置数据源 父类已经挤成了协议 这里直接重写父类的方法就可以了
extension XSHomeViewController{

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statuses.statusList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // 单个微博视图模型
        let statusVM = statuses.statusList[indexPath.row]
        
        // 区分原创微博和转发微博
        let cellID = (statusVM.status.retweeted_status != nil) ? retweetedCellID : originalCellID
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! XSStatusCell
        // 赋值
        cell.statusModel = statusVM
        
        
        // 设置URL 点击代理
        cell.urlSelectedDelegate = self

        // 将下面设置cell属性的工作交给cell视图模型去做吧！
//        cell.statusLabel.text =  statusVM.status.text
//        cell.nameLabel.text = statusVM.status.user?.screen_name    
        
        return cell
        
    }
    
    /// 重写父类等行高方法 返回计算好的行高
    /// 这里要做的是缓存行规提升性能 所以如果写了自动行高 要注销掉
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        // 获取微博模型
        let model = statuses.statusList[indexPath.row]
        
        return model.rowHeight
    }
}


// MARK: - url链接被点击
extension XSHomeViewController: XSStatusCellDelegate {

    func xSStatusCellSelectedUrlString(cell: XSStatusCell, urlString: String) {
        // 加载一个webview显示内容
        let vc = XSWebViewController()
        vc.urlString = urlString
        navigationController?.pushViewController(vc, animated: true)
    }
}


