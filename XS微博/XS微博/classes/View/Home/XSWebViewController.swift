//
//  XSWebViewController.swift
//  XS微博
//
//  Created by 王小帅 on 2016/12/28.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import UIKit

/// 点击网址链接现实的界面
class XSWebViewController: XSBaseViewController {
    
    lazy var webView = UIWebView(frame: UIScreen.main.bounds)
    
    var urlString:String? {
        didSet{
            
            guard let urlString = urlString,
                let url = URL(string: urlString)

            else {
                return
            }
            let req = URLRequest(url: url)
            webView.loadRequest(req)
        }
    }

    override func setUpUI() {
//        super.setUpUI()
        
        view.addSubview(webView)
        
        
    }
}
