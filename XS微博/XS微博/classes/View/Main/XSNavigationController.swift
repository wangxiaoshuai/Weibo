//
//  XSNavigationController.swift
//  XS微博
//
//  Created by 王小帅 on 2016/11/6.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import UIKit

class XSNavigationController: UINavigationController {


    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        navigationController?.isNavigationBarHidden = true
    }
    
    
    /// 因为每次的push操作都要把底部隐藏起来 这一统一写在这里
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        
        /// 注意：  因为在将一个个子控制器放入 tabbar中的时候也会调用push方法 所以这里要做个判断 根控制器不用隐藏
        if childViewControllers.count > 0 {
            
            viewController.hidesBottomBarWhenPushed = true

        }
        
        super.pushViewController(viewController, animated: true)
    }
    
}
