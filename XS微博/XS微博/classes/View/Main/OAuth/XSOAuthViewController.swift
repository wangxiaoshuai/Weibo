//
//  XSOAuthViewController.swift
//  XS微博
//
//  Created by 王小帅 on 2016/11/19.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import UIKit
import SVProgressHUD

/// 用户授权界面
class XSOAuthViewController: UIViewController {

    // 展示授权界面的webView
    lazy var webView =  UIWebView()
    
    // 重写loadView方法来更改跟视图
    override func loadView() {
        
        view = webView
        view.backgroundColor = UIColor.white
        // 设置标题和返回按钮
        title = "用户授权"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "取消", style: .plain, target: self, action: #selector(closeOAuthView))
        
        // 因为要对用户授权操作做监听，所以要设置webview的代理
        webView.delegate = self
        
        // 取消webview的滚动
        webView.scrollView.isScrollEnabled = false
        
        // 设置自动填充按钮
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "自动填充", style: .plain, target: self, action: #selector(autoLogin))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // 加载授权界面
        loadAuthView()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // 取消授权
    func closeOAuthView() {
        // 停止加载动画
        SVProgressHUD.dismiss()
        
        // 关闭界面
        dismiss(animated: true, completion: nil)
    }
    
    // 加载授权界面
    func loadAuthView() {
        
        // 授权api  官方文档说如果是手机要将 https://api.weibo -> http://open.weibo
        let authUrl = "https://open.weibo.cn/oauth2/authorize?client_id=\(APPClientId)&redirect_uri=\(APPRedireUri)"
        
        // 封装成URL
        let url = URL(string: authUrl)
        // 封装URLRequest
        let req = URLRequest(url: url!)
        // 加载授权界面
        webView.loadRequest(req)
        
    }

}


// MARK: - 对webview做监听
extension XSOAuthViewController: UIWebViewDelegate{
    
    
    /// webview是否加载即将请求的界面
    ///
    /// - Parameters:
    ///   - webView: 当前的webview
    ///   - request: 加载请求
    ///   - navigationType: 加载方式  [连接 /刷新 ....]
    /// - Returns: 是否加载请求的界面
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        // 开启加载动画
        SVProgressHUD.show()
        
        // 通过输出url 可以看到 http://www.baidu.com/?code=0260aa3cdfaf5c59487db5c2828c9c7c 表示登录成功了 code码涌来请求授权
        // http://www.baidu.com/?error_uri=%2Foauth2%2Fauthorize&error=access_denied&error_description=user%20denied%20your%20request.&error_code=21330) 表示授权失败或者用户点击取消授权
        print("即将进入 ： \(request.url)")
        // 如果url 请求的是新浪的认证地址那么久显示
        if request.url?.absoluteString.hasPrefix(APPRedireUri) == false {

            return true
        }
        // 如果即将请求的是百度的地址，说明要么授权成功了要么失败了这里要做判断
        // 如果成功那么获取code码 来请求accessToken
        // 如果失败了 那么关闭当前页
        if request.url?.query?.hasPrefix("code=") == false {// 不包含code= 说明要么请求失败要么取消授权都关闭页面
        
            // 取消加载动画 关闭页面
            SVProgressHUD.dismiss()
            closeOAuthView()
            
            // return false 推出方法
            return false
        }
        
        // 到这里说明授权成功了，那么取出code码 请求accessToken
//        print(request.url?.query?.substring(from: "code=".endIndex))
        let code = request.url?.query?.substring(from: "code=".endIndex) ?? " "
        // 获取AccessToken
        XSNetworkTools.shared.getAccessToken(codeStr: code, completion: { (isSuccess) -> () in
        
            // 判断是否获取AccessToken成功
            if isSuccess {
            
                SVProgressHUD.showInfo(withStatus: "授权成功")
                
                // 以通知的方法 跳转刷新界面
                // 本意想再main类中注册通知 可是每个节目都要监听 所以在其父类 base里注册了
                NotificationCenter.default.post(
                    name: NSNotification.Name(rawValue: XSUserLoginSuccessNotifitation),
                    object: nil)
                
                // 关闭当前界面
                self.closeOAuthView()
                
            }else {
             
                SVProgressHUD.showInfo(withStatus: "授权失败")
            }
        
        })
        
        // 这里return false 因为我只要code 不需要跳转到百度
        return  false
    }
    

    
    
    /// webview已做完加载
    ///
    /// - Parameter webView: webView
    func webViewDidFinishLoad(_ webView: UIWebView) {
        // 停止加载动画
        SVProgressHUD.dismiss()
    }
    
    func autoLogin() {
        let js = "document.getElementById('userId').value = '15170438358';document.getElementById('passwd').value='284759'"
        webView.stringByEvaluatingJavaScript(from: js)
    }
}





