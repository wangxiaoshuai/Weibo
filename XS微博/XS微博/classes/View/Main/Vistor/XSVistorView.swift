//
//  XSVistorView.swift
//  XS微博
//
//  Created by 王小帅 on 2016/11/13.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import UIKit
/// 访客视图
class XSVistorView: UIView {

    // 注册按钮
    public lazy  var registerBtn = UIButton(title: "注册",
                                            imgNormal: "common_button_white_disable",
                                            imgHight: "common_button_white_disable",
                                            titleColorNor: UIColor.orange,
                                            titleColorHight: UIColor.black)
    
    
    
    // 登录按钮
    public lazy  var loginBtn = UIButton(title: "登录",
                                         imgNormal: "common_button_white_disable",
                                         imgHight: "common_button_white_disable",
                                         titleColorNor: UIColor.black,
                                         titleColorHight: UIColor.black)
    

    
    // 定义一个字典来给页面设置信息 [imageName | message]
    // 因为首页已经设置完成 所以如果 imageName == “” 就不做操作 
    // ==“” 而不用 ==nil  因为这样免去解包
    var vistorDict:[String: String]? {
        didSet{
            
            // 获取图和提示文字
            guard let imgName = vistorDict?["imageName"],
                let tipStr = vistorDict?["message"] else {
                return
            }
            
            // 如果是首页那么不操作
            if imgName == " " {
                // 加入旋转动画
                setAnimation()
                return
            }

            // 获取提示文字
            tipLabel.text = tipStr
            // 其他界面 要更改对应的图片
            iconImage.image = UIImage(named: imgName)
            // 小房子图片不需要显示
            houseImage.isHidden = true
            maskIconImage.isHidden = true
        }
    }
    
    
    /// MARK: - 设置核心动画 (只有首页需要旋转！)
    private func setAnimation(){
        // 定义基础核心动画
        let ani = CABasicAnimation(keyPath: "transform.rotation")
        // 旋转180度
        ani.toValue = 2 * M_PI
        // 旋转一圈需要 10 秒
        ani.duration = 10
        // 重复次数
        ani.repeatCount = MAXFLOAT
        
        // *** 这个属性很重要，不设置的话默认true 当重新进入界面后 动画会停止 ***
        ani.isRemovedOnCompletion = false
        // 将核心动画加入图层
        iconImage.layer.add(ani, forKey: nil)
    }
    
    
    /// MARK: - 初始化方法
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        setUpUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    /// MARK: -私有化属性
    // 背景图
    fileprivate lazy var iconImage = UIImageView(image: UIImage(named: "visitordiscover_feed_image_smallicon"))
    // 遮罩图（首页才有 让小圆圈显示一半）
    fileprivate lazy var maskIconImage = UIImageView(image: UIImage(named: "visitordiscover_feed_mask_smallicon"))
    // 小房子（首页才有）
    fileprivate lazy var houseImage = UIImageView(image: UIImage(named: "visitordiscover_feed_image_house"))
    // 文字描述
    fileprivate lazy var tipLabel = UILabel(txt: "关注一些人，注册登录之后，看看你会有什么发现！",
                                            fontSize: 16,
                                            align: .center, lines: 0, fontColor: UIColor.darkGray)
    
}


/// 设置UI
extension XSVistorView {

    func setUpUI() {
        
        self.backgroundColor = UIColor(red: 237/255, green: 237/255, blue: 237/255, alpha: 1.0)
        
        // 将控件添加到视图
        addSubview(iconImage)
        addSubview(maskIconImage)
        addSubview(houseImage)
        addSubview(tipLabel)
        addSubview(registerBtn)
        addSubview(loginBtn)
        
        // 布局控件位置
        layoutChilds()
    }
    
    
    /// 调整页面的布局
    private func layoutChilds(){
    
        // 调整布局
        // 注意：纯代码编写系统默认布局为  autoresizes  xib 默认为 autolayout
        // 这里要关闭系统默认的布局
        for v in subviews {
            v.translatesAutoresizingMaskIntoConstraints = false
        }
        
        // 开始布局
        let btnW: CGFloat = 110.0
        // 背景图
        addConstraint(NSLayoutConstraint(item: iconImage,
                                         attribute: .centerX,
                                         relatedBy: .equal,
                                         toItem: self,
                                         attribute: .centerX,
                                         multiplier: 1.0,
                                         constant: 0))
        addConstraint(NSLayoutConstraint(item: iconImage,
                                         attribute: .centerY,
                                         relatedBy: .equal,
                                         toItem: self,
                                         attribute: .centerY,
                                         multiplier: 1.0,
                                         constant: -60))
        // 小房子
        addConstraint(NSLayoutConstraint(item: houseImage,
                                         attribute: .centerX,
                                         relatedBy: .equal,
                                         toItem: iconImage,
                                         attribute: .centerX,
                                         multiplier: 1.0,
                                         constant: 0))
        addConstraint(NSLayoutConstraint(item: houseImage,
                                         attribute: .centerY,
                                         relatedBy: .equal,
                                         toItem: iconImage,
                                         attribute: .centerY,
                                         multiplier: 1.0,
                                         constant: 0))
        // 提示文字
        addConstraint(NSLayoutConstraint(item: tipLabel,
                                         attribute: .top,
                                         relatedBy: .equal,
                                         toItem: houseImage,
                                         attribute: .bottom,
                                         multiplier: 1.0,
                                         constant: 50))
        addConstraint(NSLayoutConstraint(item: tipLabel,
                                         attribute: .centerX,
                                         relatedBy: .equal,
                                         toItem: houseImage,
                                         attribute: .centerX,
                                         multiplier: 1.0,
                                         constant: 0))
        // 为了居中显示区作用要设置label的宽度  
        // 设置宽度 toitem 要设置为nil
        // attribute: notAnAttribute
        addConstraint(NSLayoutConstraint(item: tipLabel,
                                         attribute: .width,
                                         relatedBy: .equal,
                                         toItem: nil,
                                         attribute: .notAnAttribute,
                                         multiplier: 1.0,
                                         constant: 236))
        // 设置注册按钮
        addConstraint(NSLayoutConstraint(item: registerBtn,
                                         attribute: .left,
                                         relatedBy: .equal,
                                         toItem: tipLabel,
                                         attribute: .left,
                                         multiplier: 1.0,
                                         constant: 0))
        addConstraint(NSLayoutConstraint(item: registerBtn,
                                         attribute: .top,
                                         relatedBy: .equal,
                                         toItem: tipLabel,
                                         attribute: .bottom,
                                         multiplier: 1.0,
                                         constant: 20))
        // 设置按钮宽度
        addConstraint(NSLayoutConstraint(item: registerBtn,
                                         attribute: .width,
                                         relatedBy: .equal,
                                         toItem: nil,
                                         attribute: .notAnAttribute,
                                         multiplier: 1.0,
                                         constant: btnW))
        // 设置登录按钮
        addConstraint(NSLayoutConstraint(item: loginBtn,
                                         attribute: .right,
                                         relatedBy: .equal,
                                         toItem: tipLabel,
                                         attribute: .right,
                                         multiplier: 1.0,
                                         constant: 0))
        addConstraint(NSLayoutConstraint(item: loginBtn,
                                         attribute: .top,
                                         relatedBy: .equal,
                                         toItem: tipLabel,
                                         attribute: .bottom,
                                         multiplier: 1.0,
                                         constant: 20))
        // 设置按钮宽度
        addConstraint(NSLayoutConstraint(item: loginBtn,
                                         attribute: .width,
                                         relatedBy: .equal,
                                         toItem: nil,
                                         attribute: .notAnAttribute,
                                         multiplier: 1.0,
                                         constant: btnW))
        
        // 设置遮罩
        // 练习 VFL 布局方法
        // views: 映射真实控件的名称
        let dict:[String: UIView] = ["maskIconView": maskIconImage, "registerBtn": registerBtn]
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[maskIconView]-0-|",
                                                      options: [],
                                                      metrics: nil,
                                                      views: dict))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[maskIconView]-0-[registerBtn]",
                                                      options: [],
                                                      metrics: nil,
                                                      views: dict))
        
        
    }
    
    
}
