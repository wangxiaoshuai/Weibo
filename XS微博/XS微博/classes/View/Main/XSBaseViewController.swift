//
//  XSBaseViewController.swift
//  XS微博
//
//  Created by 王小帅 on 2016/11/6.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import UIKit


class XSBaseViewController: UIViewController {

    /// 展示数据的tableview
    var tableView:UITableView?
    /// 刷新控件
    public var refresh:CTRefreshControl?
    /// 标记是否为上拉刷新
    var isPullUp = false
    
    
    // 访客信息字典
    var vistorInfo:[String: String]?
    
    // 将默认的状态栏设置为隐藏 在子夜面去更改显示
    override var prefersStatusBarHidden: Bool{
        return IsHidedStatusBar
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // 设置界面的方法
        setUpUI()
        // 设置数据源方法接口 子类重写实现
        XSNetworkTools.shared.userLogon ? loadData() : ()
        
        // 注册用户授权成功通知
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(userLoginSuccess(n:)),
                                               name: NSNotification.Name(rawValue: XSUserLoginSuccessNotifitation),
                                               object: nil)
        // 监听新特性和欢迎界面的移除，控制状态栏显示
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(changeStatusBar),
                                               name: NSNotification.Name(rawValue: NewFeatureAndWeclcomRemoveFromSuperView),
                                               object: nil)
    }
    
    // 注册通知和timer一定要销毁
    deinit {
        
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // 价值数据方法 子类重写
    func loadData() {
        // 程序健壮性考虑 如果子类没有实现这个方法 那么默认要把刷新控件停止
        refresh?.endRefreshing()
        
    }
    
    // 开启刷新控件
    func beginRefresh(){
        refresh?.beginRefreshing()
    }
    
    // 刷新完成调用方法 子类根据自身重写
    func refreshCompleted() {
        refresh?.endRefreshing()
    }
}



// MARK: - 监听新特性和欢迎界面的移除，控制状态栏显示
extension XSBaseViewController {

    @objc fileprivate func changeStatusBar(){
        // 刷新状态栏
        self.setNeedsStatusBarAppearanceUpdate()
    }
}


/// 这里的方法必须写在扩展里  否则子类即使重写了 也不会执行  妈蛋 不懂为什么
extension XSBaseViewController{
    
    /// 做些基本的属性设置等，子类可以重写该方法来自定义其他属性
    func setUpUI(){
        // 根据用户登录状态来显示视图
        XSNetworkTools.shared.userLogon ? setUpTableView() : setUpVisterView()
    }
    
    
    // 设置tableview
    private func setUpTableView() {
        // 添加一个tableview
        tableView = UITableView(frame: view.bounds, style: .plain)
        // 添加进界面
        view.addSubview(tableView!)
        // 设置数据源和代理, 将方法写在extension中分离代码
        tableView?.dataSource = self
        tableView?.delegate = self
        
        // 添加下拉刷新
        refresh = CTRefreshControl()
        tableView?.addSubview(refresh!)
        // 设置刷新完成方法
        refresh?.addTarget(self, action: #selector(refreshCompleted), for: UIControlEvents.valueChanged)
    }
    
    /// 设置访客视图
    private func setUpVisterView() {
        // 访客视图
        let visterView = XSVistorView(frame: view.bounds)
        // 设置访客信息
        visterView.vistorDict = vistorInfo
        view.addSubview(visterView)
        
        // 注册和登录事件
        visterView.registerBtn.addTarget(self, action: #selector(userRegesterFunc), for: .touchUpInside)
        visterView.loginBtn.addTarget(self, action: #selector(userLoginFunc), for: .touchUpInside)
    }

}


// MARK: - 注册和登录点击
// 用通知的方式加载界面。发送全局的通知，无论在什么界面，只要token过期了，就弹出登录界面
// 负责监听的应该是 main这个控制器
extension XSBaseViewController {
    // 登录
    func userLoginFunc(){
        // 发送登录通知
        NotificationCenter.default.post(name: NSNotification.Name(XSUserLoginNotifitation), object: nil)
    }
    
    // 注册
    func userRegesterFunc(){
        // 发送注册通知
    }
    
    // 用户授权成功
    func userLoginSuccess(n: Notification){
    
        print("用户授权成功了 \(n)")
        // 重新加载页面当 view == nil， 回重新调用  loadView() -> viewDidLoad()
        view = nil
        
        // 这里注意： 当重新加载的时候 通知回被重复注册 这里要销毁
        NotificationCenter.default.removeObserver(self)
        
    }
}



// MARK: - 数据源和代理方法
extension XSBaseViewController: UITableViewDataSource,UITableViewDelegate{

    // 这里的方法都不用具体实现，放到子类中让子类根据自身需要去实现
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    /// 写这个方法等目的是  要让子控制器能重写它 因为没有 swift3.0 override 不会自动执行
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 10
    }
    
    
    
    // 当表格即将显示最后一行的时候做提前的上拉刷新 注意： 子类可能有多个组 所以要取最大的组的最后一行最判断
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        // 取出当前即将显示的row
        let row = indexPath.row
        // 获取最大的组
        let section = tableView.numberOfSections - 1
        
        // 如果没有数据就不用在判断刷新了
        if row < 0 || section < 0 {
            return
        }

        // 取出当前的组的行数 这个inSection 是取的角标 所以上面的section要-1
        let count = tableView.numberOfRows(inSection: section)
        
        // 如果即将显示的row是最大的行数了并且当前没有做上拉刷新操作 那就要开始做上拉刷新了
        // row代表的是角标 所以要count - 1
        if row == (count - 1) && !isPullUp {
            
            // 标记当前为上拉刷新
            self.isPullUp = true
            // 调用刷新完成方法 子类在该方法中去判断上拉和下拉刷新
            refreshCompleted()
        }
    }
    
}







