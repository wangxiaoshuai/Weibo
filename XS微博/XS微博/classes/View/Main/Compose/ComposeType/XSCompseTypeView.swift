//
//  XSCompseTypeView.swift
//  XS微博
//
//  Created by 王小帅 on 2016/12/15.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import UIKit
import pop

class XSCompseTypeView: UIView {

    @IBOutlet weak var backConst: NSLayoutConstraint!
    
    @IBOutlet weak var closeConst: NSLayoutConstraint!
    
    let typeButtons = [
                ["typeName":"文字", "clsName":"XSComposeTxtView"],
                ["typeName":"视频"],
                ["typeName":"图片"],
                ["typeName":"长微博"],
                ["typeName":"签到"],
                ["typeName":"更多", "funcName":"moreClick"],
                ["typeName":"直播"],
                ["typeName":"点评"]
    ]
    
    
    
    /// 存放微博类型图标的滚动试图
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    /// 单个按钮点击回调函数
    var completetionBlock:((_ clsName: String?)->())?
    
    

    /// 关闭窗口
    @IBAction func close(_ sender: Any) {
        // 动画隐藏面板
        hideAnims()
    }
    
    // 返回安妮
    @IBOutlet weak var backButton: UIButton!
    
    
    @IBAction func backBtnClick(_ sender: Any) {
        print("back item click")
        
        UIView.animate(withDuration: 0.35, animations: {
            
            // 设置scrollview的contentoffset
            self.scrollView.contentOffset = CGPoint(x: 0, y: CGFloat(0))
            
            // 底部动画
            self.backConst.constant = 0
            self.closeConst.constant = 0
            self.backButton.alpha = 0
            
            self.layoutIfNeeded()
            
        }) { (_) in
            
            self.backButton.isHidden = true
        }
    }
    
    
    // 类方法获取弹板
    class func composeTypeView() -> XSCompseTypeView {
        let nib = UINib(nibName: "XSCompseTypeView", bundle: nil)
        let v = nib.instantiate(withOwner: nil, options: nil)[0] as! XSCompseTypeView
        
        // 设置v的frame
        v.frame = UIScreen.main.bounds
        // 设置类型按钮
        v.setUpComposeTypeButtons()
        
        return v
    }
    
    
    
    /// 弹出微博类型展示面板
    func show(completetion:@escaping (_ clsName: String?)->()) {
        
        // 用属性记录block 如果当前方法不能回调 那么在其他合适的地方去回调
        completetionBlock = completetion
        
        // 保证在任何节目都能弹出 要判断当前的根视图
        guard let rootView = UIApplication.shared.keyWindow?.rootViewController else {
            return
        }
        
        // 弹出面板
        rootView.view.addSubview(self)
        // 设置动画
        showAnims()
    }
    
    
    /// 按钮点击
    func btnClick(selectedButton: XSComposeTypeButton){
        
        // 当前点击的按钮要变大 其他的变小
        clickBtnAnims(clickBtn: selectedButton)
        
        
        
    }
    
    
    /// 更多按钮点击
    func moreClick(){
        
        UIView.animate(withDuration: 0.35) {
            
            // 设置scrollview的contentoffset
            self.scrollView.contentOffset = CGPoint(x: self.scrollView.bounds.width, y: CGFloat(0))
            
            // 底部动画
            let w = UIScreen.main.bounds.width / 6
            self.backConst.constant += -w
            self.closeConst.constant += w
            self.backButton.isHidden = false
            self.backButton.alpha = 1
            
            self.layoutIfNeeded()
        }
        
    }
}

// MARK: - 微博类型面板弹出以及点击动画
fileprivate extension XSCompseTypeView {

    /// 按钮弹出动画
    func showAnims() {
        
        // 面板动画
        showViewAnims()
        // 按钮动画
        showButtonsAnims()
    }
    
    /// 按钮关闭动画
    func hideAnims() {
        // 隐藏按钮
        hideButtonsSAnims()
    }
    
    
    /// 面板动画
    func showViewAnims(){
        // 面板的透明度要设置
        let anim: POPBasicAnimation = POPBasicAnimation(propertyNamed: kPOPViewAlpha)
        
        // 设置变化值
        anim.fromValue = 0
        anim.toValue = 1
        
        // 动画持续时间
        anim.duration = 0.1
        
        // 设置动画的速度 linear easeIn ...
        //        anim.timingFunction = CAMediaTimingFunction(name: "linear")
        
        // 加入动画
        pop_add(anim, forKey: nil)
    }
    
    /// 按钮动画
    func showButtonsAnims() {
        
        // 按钮逐个弹出 注意判断当前的页 因为按钮个数会不同
        let page = Int(scrollView.contentOffset.y / scrollView.bounds.width)
        let v = scrollView.subviews[page]
        
        // 遍历当前页面的所以按钮，逐个弹出来
        for (i, btn) in v.subviews.enumerated() {
            
            // 创建弹簧动画
            let anim: POPSpringAnimation = POPSpringAnimation(propertyNamed: kPOPLayerPositionY)
            
            // 设置动画更改值
            anim.fromValue = btn.center.y + 400
            anim.toValue = btn.center.y
            
            // 设置弹簧的幅度 the range [0, 20]. Defaults to 4 以及速度
            anim.springBounciness = 8
            // range [0, 20]. Defaults to 12.
            anim.springSpeed = 8
            
            // 注意： 每个button的执行动画开始时间要分开 这样才能住个出来
            anim.beginTime = CACurrentMediaTime() + CFTimeInterval(i) * 0.025
            
            // 加入动画
            btn.pop_add(anim, forKey: nil)
        }
    }
    
    /// 隐藏按钮
    func hideButtonsSAnims() {
        // 获取当前页的buttons 住个隐藏
        let page = Int(scrollView.contentOffset.x / scrollView.bounds.width)
        let v = scrollView.subviews[page]
        
        // 遍历btn逐个添加隐藏动画
        for (i, btn) in v.subviews.enumerated() {
            
            // 定义动画
            let anim: POPSpringAnimation = POPSpringAnimation(propertyNamed: kPOPLayerPositionY)
            // 动画值
            anim.fromValue = btn.center.y
            anim.toValue = btn.center.y + 400
            //动画开始时间（关闭不需要弹簧效果了）
            anim.beginTime = CACurrentMediaTime() + CFTimeInterval(v.subviews.count - i) * 0.025
            anim.springSpeed = 8
            // 加入动画
            btn.pop_add(anim, forKey: nil)
            
            // 注意： 当最后一个按钮隐藏好后 在隐藏面板
            if (i + 1) == v.subviews.count {
                // 执行一个block 去隐藏面板
                anim.completionBlock = { _,_ in
                    self.hideViewAnims()
                }
            }
            
        }
    }
    
    /// 隐藏面板
    func hideViewAnims() {
        
        // 创建基础动画
        let anim: POPBasicAnimation = POPBasicAnimation(propertyNamed: kPOPViewAlpha)
        // 动画值
        anim.toValue = 0
        // 时间
        anim.duration = 0.1
        // 加入动画
        pop_add(anim, forKey: nil)
        // 动画之行完成后 移除面板
        anim.completionBlock = {_,_ in
            self.removeFromSuperview()
        }
    }
    
    
    /// 单个按钮点击动画  被点击的变大 其他的变小  同时都要变透明度
    func clickBtnAnims(clickBtn: XSComposeTypeButton){
    
        // 获取当前视图
        let page = Int(scrollView.contentOffset.x / scrollView.bounds.width)
        let v = scrollView.subviews[page]
        
        
        // 遍历当前v 下的所有按钮分别设置动画
        for btn in v.subviews {
            
            // 定义动画
            // 注意： 在缩放动画中的xy缩放 对应到系统中 要用 CGPoint()对 value 对应包装
            let scaleAnim: POPBasicAnimation = POPBasicAnimation(propertyNamed: kPOPViewScaleXY)
            
            // 点击的按钮变大 1.5倍 未被点击的变小
            let scaleValue = (btn == clickBtn) ? 2 : 0.2
            let nsvalue = NSValue(cgPoint: CGPoint(x: scaleValue, y: scaleValue))
            // 设置缩放倍数
            scaleAnim.toValue = nsvalue
            scaleAnim.duration = 0.5
            // 添加动画
            btn.pop_add(scaleAnim, forKey: nil)
            
            // 设置透明度动画
            let alphaAnim:POPBasicAnimation = POPBasicAnimation(propertyNamed: kPOPViewAlpha)
            alphaAnim.toValue = 0.2
            alphaAnim.duration = 0.5
            
            btn.pop_add(alphaAnim, forKey: nil)
            
            // 监听动画完成 回调函数去探出对应的控制器
            alphaAnim.completionBlock = {_,_ in
                
                self.completetionBlock!(clickBtn.clsName)
            }
        }
    }
}


/// 设置发微博的类型按钮
fileprivate extension XSCompseTypeView {
    
    // 设置button
    func setUpComposeTypeButtons(){
        
        layoutIfNeeded()
        
//        let bounds = scrollView.bounds
        let width = UIScreen.main.bounds.width
        
        for i in 0..<2 {
            let v = UIView(frame: bounds.offsetBy(dx: CGFloat(i) * width, dy: 0))
            v.isUserInteractionEnabled = true
            
            // 创建button
            createButton(v: v, count: i * 6)
            
            scrollView.addSubview(v)
        }
        
        // 设置滚动试图的属性
        scrollView.contentSize = CGSize(width: CGFloat(2) * width, height: 0)
        scrollView.alwaysBounceHorizontal = false
        scrollView.alwaysBounceVertical = false
        scrollView.bounces = false
        scrollView.isScrollEnabled = false
        
    }
    
    
    // 根据角标创建button
    func createButton(v: UIView, count: Int) {
        
        for i in count..<(count + 6) {
            
            if i >= typeButtons.count {
                continue
            }
            
            let typeName = typeButtons[i]["typeName"] ?? "其他"
            
            let button:XSComposeTypeButton
            if typeName == "更多" {
                let actionName = typeButtons[i]["funcName"] ?? "moreClick"
                button = XSComposeTypeButton.composeTypeButton(lable: typeName)
                button.addTarget(self, action: Selector(actionName), for: .touchUpInside)

            }else{
            
                button = XSComposeTypeButton.composeTypeButton(lable: typeName)
                button.addTarget(self, action: #selector(btnClick), for: .touchUpInside)
            }
            
            // 设置按钮点击要弹出的控制器名字
            button.clsName = typeButtons[i]["clsName"]
            
            v.addSubview(button)
        }
        
        // 布局
        let margin:CGFloat = (UIScreen.main.bounds.width - 300) / 4
        
        for i in 0..<v.subviews.count {
            
            let col:CGFloat = CGFloat(i % 3)
            let row:CGFloat = CGFloat(i / 3)
            let x = margin + (col * CGFloat(margin + 100))
            let y = row * (margin + 100)
            
            let btn = v.subviews[i] as! XSComposeTypeButton
            
            btn.frame = CGRect(x: x, y: y, width: 100, height: 100)
            
        }

    }
    
    
    
}
