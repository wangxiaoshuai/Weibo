//
//  XSComposeTxtView.swift
//  XS微博
//
//  Created by 王小帅 on 2016/12/18.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import UIKit

class XSComposeTxtView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.orange
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "取消", style: UIBarButtonItemStyle.plain, target: self, action: #selector(dismisSelf))
        
    }
    
    @objc private func dismisSelf(){
        dismiss(animated: true, completion: nil)
    }

}
