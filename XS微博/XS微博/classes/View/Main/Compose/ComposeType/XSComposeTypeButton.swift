//
//  XSComposeTypeButton.swift
//  XS微博
//
//  Created by 王小帅 on 2016/12/17.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import UIKit

class XSComposeTypeButton: UIControl {

 
    @IBOutlet weak var buttonLable: UILabel!
    
    // 定义一个属性来记录当前按钮点击要弹出的控制器名字
    var clsName:String?
    
    class func composeTypeButton(lable: String) -> XSComposeTypeButton {
        
        let nib = UINib(nibName: "ComposeTypeButton", bundle: nil)
        let v = nib.instantiate(withOwner: nil, options: nil)[0] as! XSComposeTypeButton
        v.buttonLable.text = lable
        
        return v
    }
    
    
    
    

}
