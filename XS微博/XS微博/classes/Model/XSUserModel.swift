//
//  XSUserModel.swift
//  XS微博
//
//  Created by 王小帅 on 2016/11/29.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import UIKit


/// 微博用户模型 要是用KVC所以集成NSObject
class XSUserModel: NSObject {

    var id:Int64  = 0//	用户UID
    var idstr: String? //	字符串型的用户UID
    var screen_name:String?//	用户昵称
    var name:String?//	友好显示名称
    var verified_type:Int = 0 //	暂未支持
    var avatar_large:String? //	用户头像地址（大图），180×180像素
    var mbrank:Int = 0 // vip认证等级
    var profile_image_url:String?//用户头像地址（中图），50×50像素

    
    override var description: String{
        
        return yy_modelDescription()
    }
    
    
    
    /** province	int	用户所在省级ID
     city	int	用户所在城市ID
     location	string	用户所在地
     description	string	用户个人描述
     url	string	用户博客地址
     profile_url	string	用户的微博统一URL地址
     domain	string	用户的个性化域名
     weihao	string	用户的微号
     gender	string	性别，m：男、f：女、n：未知
     followers_count	int	粉丝数
     friends_count	int	关注数
     statuses_count	int	微博数
     favourites_count	int	收藏数
     created_at	string	用户创建（注册）时间
     following	boolean	暂未支持
     allow_all_act_msg	boolean	是否允许所有人给我发私信，true：是，false：否
     geo_enabled	boolean	是否允许标识用户的地理位置，true：是，false：否
     verified	boolean	是否是微博认证用户，即加V用户，true：是，false：否
     avatar_hd	string	用户头像地址（高清），高清头像原图
     verified_reason	string	认证原因
     follow_me	boolean	该用户是否关注当前登录用户，true：是，false：否
     online_status	int	用户的在线状态，0：不在线、1：在线
     bi_followers_count	int	用户的互粉数
     lang	string	用户当前的语言版本，zh-cn：简体中文，zh-tw：繁体中文，en：英语
     remark	string	用户备注信息，只有在查询用户关系时才返回此字段
     status	object	用户的最近一条微博信息字段 详细
     allow_all_comment	boolean	是否允许所有人对我的微博进行评论，true：是，false：否
     
     **/

    
}


