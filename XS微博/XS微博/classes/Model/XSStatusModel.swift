//
//  XSStatusModel.swift
//  XS微博
//
//  Created by 王小帅 on 2016/11/16.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import UIKit
import YYModel


/// 微博模型
class XSStatusModel: NSObject {

    // id  int64 基础数据类型必须有初始值。 与int相比兼容性更好 保证在非64位的机器上成功运行
    var id:Int64 = 0
    
    // text
    var text:String?
    
    // user
    var user: XSUserModel?
    
    var reposts_count:Int = 0//	转发数
    var comments_count:Int = 0	//评论数
    var attitudes_count:Int = 0	//表态数
    
    // 微博配图数组
    var pic_urls:[XSStatusPicsModel]?
    
    // 被转发微博
    var retweeted_status: XSStatusModel?
    
    // 微博日期
    var created_at:String?
    
    // 微博来源 
    var source: String?
    
    
    class func modelContainerPropertyGenericClass()->[String: AnyClass]{
        return ["pic_urls": XSStatusPicsModel.self]
    }
    
    
    // 重写可计算的description 属性
    override var description: String{
        
        return yy_modelDescription()
    }
    
}
