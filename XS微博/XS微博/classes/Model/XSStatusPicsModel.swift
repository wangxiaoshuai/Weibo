//
//  XSStatusPicsModel.swift
//  XS微博
//
//  Created by 王小帅 on 2016/12/1.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import UIKit


/// 微博配图模型
class XSStatusPicsModel: NSObject {

    
    /// 配图地址
    var thumbnail_pic:String?
    
//    var bmiddle_pic:String?	//中等尺寸图片地址，没有时不返回此字段
//    var original_pic:String? // 原始图
    
    
    
    override var description: String{
    
        return yy_modelDescription()
    }
}
