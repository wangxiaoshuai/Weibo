//
//  StatusViewModel.swift
//  XS微博
//
//  Created by 王小帅 on 2016/11/29.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import Foundation
import UIKit

/// 单条微博的视图模型  实现CustomStringConvertible 来完成描述输出
struct StatusViewModel: CustomStringConvertible {
    
    // 视图模型
    var status:XSStatusModel
    
    // 用户名子的颜色
    var userNameColor:UIColor?
    // vip图标
    var memberIcon:UIImage?
    // 认证图标
    var verifiedIcon:UIImage?
    // 评论数
    var commonStr: String?
    // 转发数
    var reteewtStr:String?
    // 点赞数
    var likeStr:String?
    
    /// 配图的尺寸
    var picsViewSize = CGSize()
    
    // 微博日期
    var createdAtStr:String?
    
    // 微博来源
    var statusSource:String?
    
    /// 微博正文
    var statusAttrText:NSAttributedString?
    // 转发微博正文
    var retweetAttrText:NSAttributedString?
    
    // 缓存行高
    var rowHeight:CGFloat = 0
    
    // 配图（原创或者被转发）
    var picUrls:[XSStatusPicsModel]? {
    
        return status.retweeted_status?.pic_urls ?? status.pic_urls
//        return []
    }
    
    
    
    /// 构造单条微博视图模型的方法
    ///
    /// - Parameter model: 单条微博的模型
    init(model: XSStatusModel) {
        self.status = model
        
        // 在处实话微博视图模型的时候就设置好个个图标，这样占用内存但是当刷新表格的时候就不用占用CPU去计算了
        // vip登记
        let mbrank = model.user?.mbrank
        if mbrank != nil {
            if  mbrank! > 0 && mbrank! < 7 {
                memberIcon = UIImage(named: "common_icon_membership_level\(mbrank!)")
                // 会员名字的颜色是黄色的
                userNameColor = UIColor.orange
            }
        }else{
            // 不是会员 对不起颜色就是黑色
            userNameColor = UIColor.black
        }
        
        // 认证等级 -1 ：没有认证， 0： 认证，  2，3，5 ：企业用户， 220 ：草根达人
        switch model.user?.verified_type ?? -1 {
        case 0:
            verifiedIcon = UIImage(named: "avatar_vip")
        case 2,3,5:
            verifiedIcon = UIImage(named: "avatar_enterprise_vip")
        case 220:
            verifiedIcon = UIImage(named: "avatar_grassroot")
        default:
            break
        }
        
        // 设置转发点赞评论
//        model.comments_count = Int(arc4random_uniform(100000)) // 测试
        commonStr = countStr(count: model.comments_count, defaultStr: "评论")
        reteewtStr = countStr(count: model.reposts_count, defaultStr: "转发")
        likeStr = countStr(count: model.attitudes_count, defaultStr: "赞")
        
        // 配图尺寸
        picsViewSize = sizeWithCount(count: picUrls?.count)
        // 微博来源
        statusSource = getStatusSource(source: model.source)
        // 微博日期
        createdAtStr = model.created_at
        
        // 转发和原创微博的文字
        let originalFont = UIFont.systemFont(ofSize: 15)
        let retweetedFont = UIFont.systemFont(ofSize: 14)
        
        // 微博正文
        statusAttrText = EmoticonTools.shared.emoctionString(str: model.text ?? " ", font: originalFont)

        // 转发微博正文
        let uname = model.retweeted_status?.user?.screen_name ?? " "
        let content = model.retweeted_status?.text ?? " "
        
        let rtext = "@" + uname + ":" + content
        retweetAttrText = EmoticonTools.shared.emoctionString(str: rtext, font: retweetedFont)
        
        // 根据model 计算行高
        updateRowHeight(model: model)
        
        
    }
    
    
    /// 缓存行高 提前计算号行高  在主页直接设置cell等高度
    ///
    /// - Parameter model: 微博模型
    mutating func updateRowHeight(model: XSStatusModel) {
        
        // 行高
        var height: CGFloat = 0
        
        // 原创微博的行高：顶部分割线视图(10) + 间距（12） + 图标（45）+ 间距（12）+ 正文（计算）+ 间距（12）+ 配图（计算） + 间距（12）+ 底部工具栏（35）
        // 转发微博的行高：顶部分割线视图(10) + 间距（12） + 图标（45）+ 间距（12）+ 正文（计算）+ 2 * 间距（12）+ 
        // 转发微博正文（计算） + 配图（计算） + 间距（12）+ 底部工具栏（35）
        
        // 定义常量
        let margin:CGFloat = 12
        let tooView:CGFloat = 10
        let iconHeight:CGFloat = 45
        let toolbarHeight:CGFloat = 35
        
        // 获取lable的高度限制size
        let screenW:CGFloat = UIScreen.main.bounds.width
        let size = CGSize(width: screenW - 2 * margin, height: CGFloat(MAXFLOAT))
        
        // 开始计算
        height = tooView + margin + iconHeight + margin
        
        // 如果有正文在加高度
        if let text = statusAttrText {
            
            height += text.boundingRect(with: size, options: [.usesLineFragmentOrigin], context: nil).height
        }
        
        // 如果有转发微博那么加上转发微博的正文高度
        if model.retweeted_status != nil {
            
            height += 2 * margin
            // 通过nsstring的方法来获取正文的高度
            // 注意： 这里一定要用计算过的转发文本 否则 你懂的。。。。。。。
            if let text = retweetAttrText {
                
                height += text.boundingRect(with: size, options: [.usesLineFragmentOrigin], context: nil).height
            }
        }
        
        // 计算配图高度
        height += picsViewSize.height
        height += margin
        
        // 底部工具栏高度
        height += toolbarHeight
        
        // 赋值给属性
        rowHeight = height
        
    }
    
    
    
    
    /// 根据单张照片的尺寸来修改配图视图的尺寸
    ///
    /// - Parameter image: 单张图片
    /// - Returns: 修改后的配图视图尺寸
    mutating func updateSignalPicViewSize(image: UIImage) {
        
        // 图片等尺寸
        var size = image.size
        // 高度加2倍等间距
        size.height +=  XSStatusPicsViewOutterMargin
        // 修改视图尺寸
        self.picsViewSize = size
    }
    
    
    
    
    /// 获取微博的来源 <a href="http://weibo.com" rel="nofollow">新浪微博</a>
    ///
    /// - Parameter source: <a href="http://weibo.com" rel="nofollow">新浪微博</a>
    /// - Returns: 截取后的字符串
    private func getStatusSource(source: String?) -> String{
    
        guard let source = source,
            let result = source.getSource()
            // 开始range
            //let st = source.range(of: ">"),
            // 结束range
            //let ed = source.range(of: "</a>") 
        else {
            
            return "来自未知星球"
        }
        // 截取
//        let result = source.substring(with: (st.upperBound)..<(ed.lowerBound))
        
        return "来自\(result.source) "
    }
    
    
    
    /// 根据配图的个数计算配图视图的尺寸
    ///
    /// - Parameter count: 配图个数
    /// - Returns: 计算后的配图视图尺寸
    private func sizeWithCount(count: Int?) -> CGSize{
    
        // 如果没有配图 那么返回0
        if count == 0 || count == nil {
            return CGSize()
        }
        
        // 行数: 这个公式自己试出来的
        let row = (count! - 1) / 3 + 1
        
        // 获取配图视图的高度
        var height = XSStatusPicsViewOutterMargin
        height += CGFloat(row) * XSStatusPicsViewItemWH
        height += CGFloat(row - 1) * XSStatusPicsViewInnerMargin
        
        return CGSize(width: XSStatusPicsViewItemWH, height: height)
    }
    
    
    /// 根据数量来返回显示内容
    ///
    /// - Parameters:
    ///   - count: 数量
    ///   - defaultStr: 默认显示
    /// - Returns: 指定显示
    private func countStr(count:Int, defaultStr: String)->String{
    
        // == 0 显示默认的
        if count == 0 {
            return defaultStr
        }
        
        // 小于10000 直接显示
        if count < 10000 {
            return count.description
        }
        
        // 大于10000 保留两位小数
        return String(format: "%.02f 万", Double(count) / 10000)
    }
    

    var description: String{
        return status.description
    }
    
    
    
    
}
