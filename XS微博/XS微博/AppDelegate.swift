//
//  AppDelegate.swift
//  XS微博
//
//  Created by 王小帅 on 2016/11/6.
//  Copyright © 2016年 王小帅. All rights reserved.
//

import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

//    func allow

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // 这里为app 请求通知权限[上方的提示条／声音／提示未读]
        // 注意： ios10开始请求方式发生了改变 万幸 xcode会自动帮我提示
        // 如果用原有的方法依然可以运行，但是会缺失了很多ios10特有的功能[carplay/带视频通知／在通知中快捷回复]
//        let settings = UIUserNotificationSettings.init(types: [.alert, .badge, .sound], categories: nil)
//        application.registerUserNotificationSettings(settings)
       
        // 这些代码是xcode 提示多
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound, .carPlay]){
                (success, error) in
                print("授权 \(success) ")
            }
        } else {
            // ios10 之前的版本
            let settings = UIUserNotificationSettings.init(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        
        // 指定窗口跟控制器
        window = UIWindow()
        // background color
        window?.backgroundColor = UIColor.white
        // root vc
        window?.rootViewController = XSMainController()
        // make visable
        window?.makeKeyAndVisible()
        
        
        
        return true
    }
    
    // 设置状态栏
    
    
}
